# Améliorations apportées au projet 


## Optimiser le nommage

**Utilisation des noms significatifs pour les fonctions, les variables et les classes pour éviter toute confusion lors de l’utilisation.**

- Variables :

montantVirement -> montantVersement dans la classe Versement

UtilisateurRepository rep2 -> UtilisateurRepository utilisateurRepository

- Classes : 

AutiService -> AuditService

**Association des classes à des tables avec un nom qui génère une confusion de sens  .**

AuditVersement  associe à AuditVirement table -> AuditVersement

AudiVirement associe à Audit table -> AuditVirement


## Refactorisation

- Suppression de l’importation des bibliothèques non utilisées .

- Utilisation de la bibliothèque Lombok pour générer automatiquement les getters , les setters et les constructeurs .

- Déplacement du chargement initiale des données de la classe principale vers une nouvelle Classe DataLoad.

- Création des nouveaux contrôleurs et séparation des contrôleurs mixées (UtilisateurController , CompteUtilisateur ..)

- Création des nouveaux DTOs pour faciliter l'affichage des informations (VersementDto , UtilisateurDto)

- Création des nouveaux Services (CompteService,UtilisationService ..)

- Creation des nouvelles exceptions (MemeNrCompteException )


## Anomalies détectées dans le traitement des cas d'utilisation :

- Autorisation des transactions entre un même compte.

- Autorisation des transactions même si le solde n'est pas suffisant. 


## Implementation du use case Versement : 

Creation de service , repository , dto, controleur , mapper etc..

## Tests unitaires

Utilisation des simples JUNIT tests pour les repositories et des tests avec mockMvc pour tester les controleurs.

## Securite 

L'ajout d'une simple couche de sécurité basée sur le JWT Authentification pour génerer et valider les tokens.

## IDE utilisé : STS4

--------------------------------------------------------------------------------------------------------------------------------------



## Description :

Un virement est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau usecase versement. Le Versement est un dépôt d'agent sur un compte donné . 

Le versement est une opération trés utile lors des transfert de cash .
 
Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité .
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB .  


## Assignement :

* Le code présente des anomalies de qualité (bonnes pratiques , abstraction , lisibilité ...) et des bugs . 
    * localiser le maximum 
    * Essayer d'améliorer la qualité du code .    
    * Essayer de résoudre les bugs détectés. 
* Implementer le use case `Versement` 
* Ajouter des tests unitaires.  
* **Nice to have** : Ajouter une couche de sécurité 

## How to use 
To build the projet you will need : 
* Java 11+ 
* Maven

Build command : 
```
mvn clean install
```

Run command : 
```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```   
* Do the stuff .
* Send us the link .
