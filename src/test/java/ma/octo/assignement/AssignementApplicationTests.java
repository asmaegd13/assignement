package ma.octo.assignement;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import  ma.octo.assignement.web.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AssignementApplicationTests {
	@Autowired
	VirementController VirementController;
	
	@Autowired
	VersementController VersementController;
	
	@Autowired
	UtilisateurController UtilisateurController;
	
	@Autowired
	CompteController CompteController;
	
	@Test
	void contextLoads() {
		assertThat(VirementController).isNotNull();
		assertThat(VersementController).isNotNull();
		assertThat(UtilisateurController).isNotNull();
		assertThat(CompteController).isNotNull();

	}

}
