package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import javax.transaction.Transactional;

import ma.octo.assignement.domain.Utilisateur;



@ExtendWith(SpringExtension.class)
@Transactional
@DataJpaTest
public class UtilisateurRepositoryTest {


	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private TestEntityManager entityManager;

	private Utilisateur utilisateur_test;


	public void setup() {


	}

	@Test
	public void save() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");
		entityManager.persist(utilisateur_test);
		assertThat(utilisateurRepository.findById(utilisateur_test.getId())).isNotEmpty();


	}

	@Test
	public void findOne() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");
		entityManager.persist(utilisateur_test);
		Utilisateur found = utilisateurRepository.findById(utilisateur_test.getId()).get();
		assertThat(found).isEqualTo(utilisateur_test);
	}


	@Test
	public void findAll() {

		Iterable<Utilisateur> utilisateurs= utilisateurRepository.findAll();

		assertThat(utilisateurs).size().isGreaterThan(0);


	}


	@Test
	public void delete() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");
		entityManager.persistAndFlush(utilisateur_test);

		utilisateurRepository.deleteById(utilisateur_test.getId());
		assertThat(utilisateurRepository.findById(utilisateur_test.getId())).isEmpty();
	}
}