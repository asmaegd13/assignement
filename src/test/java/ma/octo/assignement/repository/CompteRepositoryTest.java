package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import javax.transaction.Transactional;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Compte;



@ExtendWith(SpringExtension.class)
@Transactional
@DataJpaTest
public class CompteRepositoryTest {


	@Autowired
	private CompteRepository compteRepository;

	@Autowired
	private TestEntityManager entityManager;

	private Utilisateur utilisateur_test;


	@Test
	public void save() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");

		entityManager.persist(utilisateur_test);

		Compte compte_test = new Compte();
		compte_test.setNrCompte("010000A000001000");
		compte_test.setRib("RIB1");
		compte_test.setSolde(BigDecimal.valueOf(200000L));
		compte_test.setUtilisateur(utilisateur_test);

		entityManager.persist(compte_test);
		assertThat(compteRepository.findById(compte_test.getId())).isNotEmpty();

	}

	@Test
	public void findOne() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");

		entityManager.persist(utilisateur_test);

		Compte compte_test = new Compte();
		compte_test.setNrCompte("010000A000001000");
		compte_test.setRib("RIB1");
		compte_test.setSolde(BigDecimal.valueOf(200000L));
		compte_test.setUtilisateur(utilisateur_test);

		entityManager.persist(compte_test);
		Compte found = compteRepository.findById(compte_test.getId()).get();
		assertThat(found).isEqualTo(compte_test);

	}


	@Test
	public void findAll() {


		Iterable<Compte> comptes= compteRepository.findAll();

		assertThat(comptes).size().isGreaterThan(0);

	}


	@Test
	public void delete() {

		utilisateur_test = new Utilisateur();
		utilisateur_test.setUsername("user_test");
		utilisateur_test.setLastname("last_test");
		utilisateur_test.setFirstname("first_test");
		utilisateur_test.setGender("Male");
		entityManager.persist(utilisateur_test);

		Compte compte_test = new Compte();
		compte_test.setNrCompte("010000A000001000");
		compte_test.setRib("RIB1");
		compte_test.setSolde(BigDecimal.valueOf(200000L));
		compte_test.setUtilisateur(null);

		entityManager.persist(compte_test);
		compteRepository.delete(compte_test);
		assertThat(compteRepository.findById(compte_test.getId())).isEmpty();
	}
}