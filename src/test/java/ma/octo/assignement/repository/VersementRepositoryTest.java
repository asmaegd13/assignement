package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import javax.transaction.Transactional;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
public class VersementRepositoryTest {

	@Autowired
	private VersementRepository versementRepository;

	@Autowired
	private TestEntityManager entityManager;


	@Test
	public void save() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);


		Versement versement_test = new Versement();
		versement_test.setMontantVersement(BigDecimal.TEN);
		versement_test.setCompteBeneficiaire(compte1);
		versement_test.setNomEmetteur("Emetteur test");
		versement_test.setDateExecution(new Date());
		versement_test.setMotifVersement("Assignment 2021 TEST");

		entityManager.persist(versement_test);
		assertThat(versementRepository.findById(versement_test.getId())).isNotEmpty();


	}

	@Test
	public void delete() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);



		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);


		Versement versement_test = new Versement();
		versement_test.setMontantVersement(BigDecimal.TEN);
		versement_test.setCompteBeneficiaire(compte1);
		versement_test.setNomEmetteur("Emetteur test");
		versement_test.setDateExecution(new Date());
		versement_test.setMotifVersement("Assignment 2021 TEST");

		entityManager.persist(versement_test);
		versementRepository.delete(versement_test);
		assertThat(versementRepository.findById(versement_test.getId())).isEmpty();

	}
	@Test
	public void findOne() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);


		Versement versement_test = new Versement();
		versement_test.setMontantVersement(BigDecimal.TEN);
		versement_test.setCompteBeneficiaire(compte1);
		versement_test.setNomEmetteur("Emetteur test");
		versement_test.setDateExecution(new Date());
		versement_test.setMotifVersement("Assignment 2021 TEST");

		entityManager.persist(versement_test);
		Versement found = versementRepository.findById(versement_test.getId()).get();
		assertThat(found).isEqualTo(versement_test);
	}

	@Test
	public void findAll() {

		Iterable<Versement> versements= versementRepository.findAll();

		assertThat(versements).size().isGreaterThan(0);
	}


}