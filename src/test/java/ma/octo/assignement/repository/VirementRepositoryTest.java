package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import javax.transaction.Transactional;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
public class VirementRepositoryTest {

	@Autowired
	private VirementRepository virementRepository;

	@Autowired
	private TestEntityManager entityManager;


	@Test
	public void save() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		entityManager.persist(utilisateur2);


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		entityManager.persist(compte2);

		Virement virement_test = new Virement();
		virement_test.setMontantVirement(BigDecimal.TEN);
		virement_test.setCompteBeneficiaire(compte2);
		virement_test.setCompteEmetteur(compte1);
		virement_test.setDateExecution(new Date());
		virement_test.setMotifVirement("Assignment 2021 TEST");

		entityManager.persist(virement_test);
		assertThat(virementRepository.findById(virement_test.getId())).isNotEmpty();


	}

	@Test
	public void delete() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		entityManager.persist(utilisateur2);


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		entityManager.persist(compte2);

		Virement virement_test = new Virement();
		virement_test.setMontantVirement(BigDecimal.TEN);
		virement_test.setCompteBeneficiaire(compte2);
		virement_test.setCompteEmetteur(compte1);
		virement_test.setDateExecution(new Date());
		virement_test.setMotifVirement("Assignment 2021 TEST");

		entityManager.persist(virement_test);
		virementRepository.delete(virement_test);
		assertThat(virementRepository.findById(virement_test.getId())).isEmpty();

	}
	@Test
	public void findOne() {

		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");

		entityManager.persist(utilisateur1);

		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		entityManager.persist(utilisateur2);


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		entityManager.persist(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		entityManager.persist(compte2);

		Virement virement_test = new Virement();
		virement_test.setMontantVirement(BigDecimal.TEN);
		virement_test.setCompteBeneficiaire(compte2);
		virement_test.setCompteEmetteur(compte1);
		virement_test.setDateExecution(new Date());
		virement_test.setMotifVirement("Assignment 2021 TEST");

		entityManager.persist(virement_test);
		Virement found = virementRepository.findById(virement_test.getId()).get();
		assertThat(found).isEqualTo(virement_test);
	}

	@Test
	public void findAll() {

		Iterable<Virement> virements= virementRepository.findAll();

		assertThat(virements).size().isGreaterThan(0);
	}


}