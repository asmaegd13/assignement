package ma.octo.assignement.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class VersementControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	VersementRepository versementRepository;
	public static String token = "";  // Generated token for test

	@Test
	public void findAll() throws Exception {
		this.mockMvc.perform(get("/lister_Versements").header("authorization",token))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	void createTransactionTest() throws Exception {
		VersementDto versementDto = new VersementDto("user_test","Nom_test","RIB_test","motif_test", new BigDecimal(250));
		mockMvc.perform(post("/executerVersement", 42L)
				.contentType("application/json").header("authorization",token)
				.param("createTransaction", "true")
				.content(objectMapper.writeValueAsString(versementDto)))
		.andExpect(status().isOk());
		assertThat(versementDto).isNotNull();
	}


}
