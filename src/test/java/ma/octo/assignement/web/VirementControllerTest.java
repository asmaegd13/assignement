package ma.octo.assignement.web;


import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.VirementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class VirementControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	VirementRepository virementRepository;
	public static String token = ""; //Generated token for test

	@Test
	public void findAll() throws Exception {
		this.mockMvc.perform(get("/lister_virements").header("authorization",token))
		.andDo(print())
		.andExpect(status().isOk());
	}
	@Test
	void createTransactionTest() throws Exception {
		VirementDto virementDto = new VirementDto("010000B025001000","010000A000001000","motif",new BigDecimal(250));
		mockMvc.perform(post("/executerVirements", 42L)
				.contentType("application/json").header("authorization",token)
				.param("createTransaction", "true")
				.content(objectMapper.writeValueAsString(virementDto)))
		.andExpect(status().isOk());
		assertThat(virementDto).isNotNull();
	}


}
