package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class UtilisateurDto {
	private String user;
	private String password;
	private String token;
}