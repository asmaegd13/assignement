package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.octo.assignement.domain.Utilisateur;

@Getter @Setter @NoArgsConstructor
public class VersementDto {
	private String nomEmetteur;
	private String prenomEmetteur;

	private String rib;
	private String motifVersement;
	private BigDecimal montantVersement;
	private Date dateExecution;
	public VersementDto(String nomEmetteur, String prenomEmetteur, String rib, String motifVersement, BigDecimal montantVersement) {
		this.nomEmetteur = nomEmetteur;
		this.prenomEmetteur = prenomEmetteur;
		this.rib = rib;
		this.motifVersement = motifVersement;
		this.montantVersement = montantVersement;
	}

}
