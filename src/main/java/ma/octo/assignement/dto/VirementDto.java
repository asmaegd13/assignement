package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class VirementDto {
	private String nrCompteEmetteur;
	private String nrCompteBeneficiaire;
	private String motif;
	private BigDecimal montantVirement;
	private Date date;

	public VirementDto(String nrCompteEmetteur, String nrCompteBeneficiaire, String motif, BigDecimal montantVirement) {
		this.nrCompteEmetteur = nrCompteEmetteur;
		this.nrCompteBeneficiaire = nrCompteBeneficiaire;
		this.motif = motif;
		this.montantVirement = montantVirement;
	}


}
