package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.domain.Compte;

public class CompteService {

	@Autowired
	private CompteRepository compteRepository;

	public List<Compte> getAllComptes() {
		List<Compte> comptes=new ArrayList<>();
		compteRepository.findAll().forEach(comptes::add);
		return comptes;
	}

	public Optional<Compte> getCompte(Long id) {
		return compteRepository.findById(id);
	}

	public void ajouterCompte(Compte compte) {
		compteRepository.save(compte);
	}

	public void modifierCompte(Long id, Compte compte) {
		compteRepository.save(compte);
	}

	public void supprimerCompte(Long id) {
		compteRepository.deleteById(id);
	}
}
