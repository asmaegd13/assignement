package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.repository.UtilisateurRepository;

import ma.octo.assignement.domain.Utilisateur;
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	public List<Utilisateur> getAllUtilisateurs() {
		List<Utilisateur> utilisateurs=new ArrayList<>();
		utilisateurRepository.findAll().forEach(utilisateurs::add);
		return utilisateurs;
	}
	public Optional<Utilisateur> getUtilisateur(Long id) {
		return utilisateurRepository.findById(id);
	}
	public void ajouterUtilisateur(Utilisateur utilisateur) {
		utilisateurRepository.save(utilisateur);
	}
	public void modifierUtilisateur(Long id, Utilisateur utilisateur) {
		utilisateurRepository.save(utilisateur);
	}
	public void supprimerUtilisateur(Long id) {
		utilisateurRepository.deleteById(id);
	}
}
