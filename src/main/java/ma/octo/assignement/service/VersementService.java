package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public class VersementService {

	@Autowired
	private VersementRepository versementRepository;

	public List<Versement> getAllVersements() {
		List<Versement> versements=new ArrayList<>();
		versementRepository.findAll().forEach(versements::add);
		return versements;
	}
	public Optional<Versement> getVersement(Long id) {
		return versementRepository.findById(id);
	}
	public void ajouterUtilisateur(Versement utilisateur) {
		versementRepository.save(utilisateur);
	}
	public void modifierVersement(Long id, Versement utilisateur) {
		versementRepository.save(utilisateur);
	}
	public void supprimerVersement(Long id) {
		versementRepository.deleteById(id);
	}
	public void createTransaction(@RequestBody VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	}
}
