package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MemeNrCompteExeption;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public class VirementService {

	@Autowired
	private VirementRepository virementRepository;

	public List<Virement> getAllVirements() {
		List<Virement> virements=new ArrayList<>();
		virementRepository.findAll().forEach(virements::add);
		return virements;
	}


	public void createTransaction(@RequestBody VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException, MemeNrCompteExeption {
	}

	public Optional<Virement> getVirsement(Long id) {
		return virementRepository.findById(id);
	}
	public void ajouterVirement(Virement virement) {
		virementRepository.save(virement);
	}
	public void modifierVirement(Long id, Virement virement) {
		virementRepository.save(virement);
	}
	public void supprimerVirement(Long id) {
		virementRepository.deleteById(id);
	}




}
