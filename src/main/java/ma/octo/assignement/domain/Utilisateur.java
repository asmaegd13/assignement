package ma.octo.assignement.domain;


import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "UTILISATEUR")
@Getter @Setter @NoArgsConstructor
public class Utilisateur implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 10, nullable = false, unique = true)
	private String username;

	@Column(length = 10, nullable = false)
	private String gender;

	@Column(length = 60, nullable = false)
	private String lastname;

	@Column(length = 60, nullable = false)
	private String firstname;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Compte> compte;

}
