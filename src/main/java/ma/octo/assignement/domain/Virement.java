package ma.octo.assignement.domain;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "VIREMENT")
@Getter @Setter @NoArgsConstructor
public class Virement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(precision = 16, scale = 2, nullable = false)
	private BigDecimal montantVirement;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecution;

	@ManyToOne
	private Compte compteEmetteur;

	@ManyToOne
	private Compte compteBeneficiaire;

	@Column(length = 200)
	private String motifVirement;



	public Virement(Date dateExecution,Compte compteEmetteur,Compte compteBeneficiaire,String motifVirement,BigDecimal montantVirement) {
		this.compteBeneficiaire=compteBeneficiaire;
		this.compteEmetteur=compteEmetteur;
		this.dateExecution=dateExecution;
		this.motifVirement=motifVirement;
		this.montantVirement=montantVirement;
	}

}
