package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "AUDIT_VIREMENT") // name of table must be changed to AUDIT_VERSEMENT
@Getter @Setter @NoArgsConstructor
public class AuditVersement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 100)
	private String message;

	@Enumerated(EnumType.STRING)
	private EventType eventType;


}
