package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MemeNrCompteExeption;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import ma.octo.assignement.service.VirementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/virements")
public
class VirementController {

	public static final int MONTANT_MAXIMAL = 10000;

	@Autowired
	VirementService virementService;


	@GetMapping("lister_virements")
	List<Virement> getVirements() {
		return virementService.getAllVirements();

	}

	@PostMapping("/executerVirements")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity createTransaction(@RequestBody VirementDto virementDto) {
		try {
			virementService.createTransaction(virementDto);
		}catch(CompteNonExistantException e){
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body("Compte n existe pas ");
		}
		catch(SoldeDisponibleInsuffisantException e){
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body("le solde disponible est insuffisant");
		}
		catch (MemeNrCompteExeption e) {
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body(" transaction vers le même compte");
		}
		catch(TransactionException e){
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body("Erreur Transaction");
		} 

		return ResponseEntity.ok(HttpStatus.OK);
	}


}
