package ma.octo.assignement.web;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
@RestController
public class CompteController {

	@Autowired
	private CompteService compteService;

	@RequestMapping("/comptes")
	public List<Compte> getComptes(){
		return compteService.getAllComptes();
	}
	@RequestMapping("/comptes/{id}")
	public Optional<Compte> getCompte(@PathVariable Long id){
		return compteService.getCompte(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/comptes")
	public void ajouterCompte(@RequestBody Compte utilisateur) {
		compteService.ajouterCompte(utilisateur);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/comptes/{id}")
	public void modifierCompte(@RequestBody Compte utilisateur, @PathVariable Long id) {
		compteService.modifierCompte(id, utilisateur);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/comptes/{id}")
	public void supprimerCompte(@PathVariable Long id) {
		compteService.supprimerCompte(id);
	}
}