package ma.octo.assignement.web;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.UtilisateurService;
@RestController
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;

	@RequestMapping("/utilisateurs")
	public List<Utilisateur> getUtilisateurs(){
		return utilisateurService.getAllUtilisateurs();
	}
	@RequestMapping("/utilisateurs/{id}")
	public Optional<Utilisateur> getUtilisateur(@PathVariable Long id){
		return utilisateurService.getUtilisateur(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/utilisateurs")
	public void ajouterUtilisateur(@RequestBody Utilisateur utilisateur) {
		utilisateurService.ajouterUtilisateur(utilisateur);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/utilisateurs/{id}")
	public void modifierUtilisateur(@RequestBody Utilisateur utilisateur, @PathVariable Long id) {
		utilisateurService.modifierUtilisateur(id, utilisateur);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/utilisateurs/{id}")
	public void supprimerUtilisateur(@PathVariable Long id) {
		utilisateurService.supprimerUtilisateur(id);
	}

	@PostMapping("user")
	public UtilisateurDto login(@RequestParam("user") String username, @RequestParam("password") String pwd) {

		String token = getJWTToken(username);
		UtilisateurDto user = new UtilisateurDto();
		user.setUser(username);
		user.setToken(token);
		return user;

	}

	private String getJWTToken(String username) {

		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
		String secretKey = "mySecretKey";

		String token = Jwts.builder().setSubject(username).setId("softtekJWT").claim("authorities",grantedAuthorities.stream().map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList())).setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + 600000)).signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}