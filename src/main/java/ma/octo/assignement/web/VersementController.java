package ma.octo.assignement.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;

@RestController(value = "/versements")
public class VersementController {

	public static final int MONTANT_MAXIMAL = 10000;

	Logger LOGGER = LoggerFactory.getLogger(VersementController.class);

	@Autowired
	VersementService versementService;

	@GetMapping("lister_versements")
	List<Versement> getVersements() {
		return versementService.getAllVersements();

	}

	@PostMapping("/executerVersements")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity createTransaction(@RequestBody VersementDto versementDto) throws SoldeDisponibleInsuffisantException {
		try{
			versementService.createTransaction(versementDto);
		}catch(CompteNonExistantException e){
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body("Compte n existe pas");
		}
		catch(TransactionException e){
			e.printStackTrace();
			return ResponseEntity
					.status(HttpStatus.FORBIDDEN)
					.body("Error Transaction");
		}

		return ResponseEntity.ok(HttpStatus.OK);
	}

}
